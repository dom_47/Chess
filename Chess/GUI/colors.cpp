#include "colors.h"

QColor Colors::light() {
    return QColor(154,204,255);

}

QColor Colors::dark() {
    return QColor(0,101,159);
}

QColor Colors::background() {
    return QColor(153,153,153);
}

QColor Colors::highlight() {
    return QColor(255,223,50);
}

QColor Colors::move() {
    return QColor(185,255,50);
}

QColor Colors::special() {
    return QColor(255,50,50);
}
